import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return(
            <div style={footerStyle}>
                <p>Do the maths on Calculinator.com</p>
            </div>
        )
    }
}

const footerStyle = {
    position: 'fixed',
    left: '0',
    bottom: '0',
    width: '100%',
    backgroundColor: '#1abc9c',
    color: 'white',
    textAlign: 'center'
}

export default Footer;