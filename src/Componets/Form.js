import React, { Component } from 'react';
import axios from 'axios'

class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            num1: 0,
            num2: 0,
            result: 0,
            url: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(event) {
        if (event.target.name === "num1") {
            this.setState({ num1: event.target.value });
        }
        if (event.target.name === "num2") {
            this.setState({ num2: event.target.value });
        }
    }
    handleClick(id) {
        if (id === 1) {
            this.setState({ url: "http://ec2-3-137-161-175.us-east-2.compute.amazonaws.com/sumar" });
        }
        if (id === 2) {
            this.setState({ url: "http://ec2-3-12-163-88.us-east-2.compute.amazonaws.com/restar" });
        }
        if (id === 3) {
            this.setState({ url: "http://ec2-35-174-242-230.compute-1.amazonaws.com/multiplicar" });
        }
        if (id === 4) {
            this.setState({ url: "http://ec2-54-202-164-34.us-west-2.compute.amazonaws.com/divide" });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();


        if (this.state.num1 > 0 && this.state.num2 > 0) {


            const bodyParams = { number1: this.state.num1, number2: this.state.num2 };

            axios(this.state.url, {
                method: 'POST',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                },
                data:bodyParams
            }).then((response) => {
                this.setState({ result: "El resultado es: " + response.data.resultado });
            }).catch((e) => {
                this.setState({ result: "Por favor revisa los números" });
            });

        } else {
            this.setState({ result: "Debes colocar un valores mayores a 0" });
        }
    }

    render() {
        const { num1, num2 } = this.state
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div style={formStyle}>
                        <label style={labelStyle}>Number 1</label>
                        <input type="number" name="num1" value={this.state.num1} onChange={this.handleChange} placeholder="Insert a number" />
                        <label style={labelStyle}>Number 2</label>
                        <input type="number" name="num2" value={this.state.num2} onChange={this.handleChange} placeholder="Insert a number" />
                    </div>
                    <div>
                        {/* <input style={buttonStyle} type="submit" value="Sumar" />
                        <input style={buttonStyle} type="submit" value="Restar" />
                        <input style={buttonStyle} type="submit" value="Multiplicar" />
                        <input style={buttonStyle} type="submit" value="Dividir" /> */}
                        <button style={buttonStyle} onClick={(e) => this.handleClick(1)}>Sumar</button>
                        <button style={buttonStyle} onClick={(e) => this.handleClick(2)}>Restar</button>
                        <button style={buttonStyle} onClick={(e) => this.handleClick(3)}>Multiplicar</button>
                        <button style={buttonStyle} onClick={(e) => this.handleClick(4)}>Dividir</button>
                    </div>
                </form>
                <br></br>
                <br></br>
                <label>{this.state.result}</label>
            </div>
        )
    }
}



const formStyle = {
    padding: '2px',
    margin: '60px',
}

const labelStyle = {
    margin: '10px',
    padding: '2px'
}

const buttonStyle = {
    display: 'inline-block',
    padding: '15px 25px',
    marginTop: '45px',
    marginRight: '5px',
    marginLeft: '5px',
    fontSize: '24px',
    cursor: 'pointer',
    textAlign: 'center',
    textDecoration: 'none',
    outline: 'none',
    color: '#fff',
    backgroundColor: '#4CAF50',
    border: 'none',
    borderRadius: '15px',
    boxShadow: '0 9px #999',
    hover: {
        backgroundColor: '#3e8e41'
    },
    active: {
        backgroundColor: '#3e8e41',
        boxShadow: '0 5px #666',
        transform: 'translateY(4px)',
    }
}

export default Form;