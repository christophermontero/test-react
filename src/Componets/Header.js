import React, { Component } from 'react';

class Header extends Component {
    render() {
        return(
            <div style={headerStyle}>
                <h1>Calculinator</h1>
            </div>
        )
    }
}

const headerStyle = {
    padding: '40px',
    textAlign: 'center',
    background: '#1abc9c',
    color: 'white',
    fontSize: '30px',
}

export default Header;